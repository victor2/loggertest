#include "TestSink.h"
#include "Logger.h"
#include <iostream>
#include <assert.h>

using namespace std;

void producer()
{
	static int id = 0;
	int myid = id++; // TODO add synchronization?

	for (int i = 0; i < 3; i++)
	{
		Info << "producer " << myid << " message " << i;
		_sleep(5);
	}
}

struct UserData
{
	int a = 5;
	int b = 16;
	const char* secret = "This is secret user data";

	void print(std::ostream &os) const
	{
		os << "[UserData: " << a << ", " << b << ", " << secret << "]";
	}
};

int main()
{
	Info << "Info message " << 65;
	_sleep(50);
	Debug << "This is a debug message " << 68;
	_sleep(50);
	Error << "Another message " << 77;
	_sleep(50);
	Info.setContext("Context!!") << "Info message " << 65;
	_sleep(500);

	thread t1(producer);
	thread t2(producer);
	thread t3(producer);

	t1.join();
	t2.join();
	t3.join();

	map<string, string> params =
	{
		{ "Format", "%F %T" },
		{ "Type", "test" },
		{ "Level", "info" }
	};

	Info.printf("hello, printf %i", 33) << " and more ";
	Info.setContext("Hex output: ").hex(&params, 20);

	// TODO Raw code to be refactored into unit test project

	// This object will be automatically deleted by CLogger. To be used in all unit tests
	LoggerLib::CTestSink *ts = dynamic_cast<LoggerLib::CTestSink*>(LoggerLib::CLogger::createSink(params));
	assert(ts);

	string hello = "Hello world";
	Info << hello;
	_sleep(50);
	assert(ts->message.find(hello) > 0);

	UserData ud;
	Info << ud;
	_sleep(50);
	assert(ts->message.find(ud.secret) > 0);

	// TODO add a test for configuration reload on the fly

	// TODO add a test that slow log sinks don't block calling application.
	// This requires a configurable sleep() in TestSink class.

	return 0;
}
