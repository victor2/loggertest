#include "LoggerCore.h"
#include <Windows.h>
#include <string>
#include <vector>
#include "TestSink.h"
#include "FileSink.h"

using namespace std;

namespace LoggerLib
{

	CLoggerCore::CLoggerCore()
	{
		init();
		start();
	}

	CLoggerCore::~CLoggerCore()
	{
		stop();
		for (auto s : m_sinks)
		{
			delete s;
		}
	}

	// TODO replace WinAPI calls with preferred configuration library
	LPCSTR CLoggerCore::m_iniFile = ".\\logger.ini";

	string CLoggerCore::getGlobal(LPCSTR key, LPCSTR deflt)
	{
		CHAR value[512] = { 0 };
		GetPrivateProfileString("Global", key, deflt, value, sizeof(value), m_iniFile);
		return value;
	}

	vector<string> CLoggerCore::getSinks()
	{
		vector<string> sinks;
		CHAR value[256] = {0};
		GetPrivateProfileString("Global", "Sinks", "", value, sizeof(value), m_iniFile);
		for (CHAR *p = strtok(value, ", "); p; p = strtok(0, ", "))
		{
			sinks.push_back(p);
		}
		
		return sinks;
	}

	// TODO refactore to deliver entire collection of parameters, not only known ones.
	string CLoggerCore::getSinkParam(LPCSTR sink, LPCSTR key, LPCSTR deflt)
	{
		CHAR value[256] = {0};
		GetPrivateProfileString(sink, key, deflt, value, sizeof(value), m_iniFile);
		return value;
	}

	void CLoggerCore::init()
	{
		// TODO think: is this synchronization sufficient to reload configuration on the fly?
		std::lock_guard<std::mutex> lock(m_mutex);
		for (auto s : m_sinks) delete s;

		vector<string> sinks = getSinks();

		// global scope provides default values
		string gformat = getGlobal("Format", "%c");
		string gtype = getGlobal("Type", "file");
		string glevel = getGlobal("Level", "info");
		vector<string> errors;

		for (string sink : sinks)
		{
			// global parameters are used as default
			string format2 = getSinkParam(sink.c_str(), "Format", gformat.c_str());
			string type2 = getSinkParam(sink.c_str(), "Type", gtype.c_str());
			string level2 = getSinkParam(sink.c_str(), "Level", glevel.c_str());
			string fname = getSinkParam(sink.c_str(), "File");
			Msg::Level lev = Msg::fromString(level2);

			map<string, string> pars = 
			{
				{ "Format", format2 },
				{ "Type", type2 },
				{ "File", fname },
				{ "Level", level2 }
			};

			CBaseSink *bs = createSink(pars);
			if (!bs)
			{
				errors.push_back("Failed to create sink '" + sink + "' of type '" + type2 + "'");
			}
		}

		// If no sinks have been created so far, let's add a console sink 
		// and write a warning message
		if (m_sinks.size() == 0)
		{
			errors.push_back("CLogger failed to create log sinks based on configuration in '" + string(m_iniFile) + "'");
			map<string, string> console =
			{
				{ "Format", "%c" },
				{ "Type", "file" },
				{ "File", "con" },
				{ "Level", "warn" }
			};
			createSink(console);
		}

		// Dispatch the first message. 
		// Note we need to append EOL to the message here - normally it is added by CLogger
		Msg msg(Msg::INFO_MSG, "Logger configuration loaded\r\n");
		m_queue.push(msg);

		// Log all configuration errors
		for (string e : errors)
		{
			Msg msg(Msg::ERROR_MSG, e + "\r\n");
			m_queue.push(msg);
		}
		m_not_empty.notify_one();
	}

	void CLoggerCore::push(Msg &msg)
	{
		// TODO optimisation idea: if this message is exact copy of previous one,
		// increment a counter instead of pushing to the queue and then write
		// that the last message was repeated NN times
		std::lock_guard<std::mutex> lock(m_mutex);
		m_queue.push(msg);
		m_not_empty.notify_one();
	}

	void CLoggerCore::start()
	{
		m_thread = std::thread(&CLoggerCore::work, this);
	}

	void CLoggerCore::stop()
	{
		m_stopped = true;
		m_thread.join();
	}

	void CLoggerCore::work()
	{
		while (true)
		{
			// this is to copy messages from the main communuication queue
			// and release mutex
			queue<Msg> q;

			// This while loop limits the scope of mutex lock - it should be released 
			// before we start delivering log message to all sinks.
			while (!m_stopped)
			{
				std::unique_lock<std::mutex> ulock(m_mutex);
				if (m_not_empty.wait_for(ulock, std::chrono::seconds(1)) == std::cv_status::no_timeout)
				{
					if (!m_queue.empty())
					{
						swap(q, m_queue);
						break;
					}
				}
			}

			if (m_stopped)
			{
				break;
			}

			// TODO for safety sake, check how many log messages are pending
			// If too many, there are slow sinks that should be excluded

			while (!q.empty())
			{
				Msg msg = q.front();
				q.pop();

				for (CBaseSink *sink : m_sinks)
				{
					if (msg.level >= sink->getLevel())
					{
						// TODO for safety sake, if deliver() fails, this sink should be marked
						// as not working well and possibly excluded
						sink->deliver(msg);
					}
				}
			}
		}
		std::cout << "Terminated" << std::endl;
	}
	
	CBaseSink *CLoggerCore::createSink(std::map<std::string, std::string> &params)
	{
		string type = params["Type"];
		string fmt = params["Format"];
		Msg::Level level = Msg::fromString(params["Level"]);
		CBaseSink *bs = nullptr;
		if (type == "test")
		{
			bs = new CTestSink(level, fmt);
		}
		else if (type == "file")
		{
			bs = new CFileSink(params["File"], level, fmt);
		}

		if (bs)
		{
			m_sinks.push_back(bs);
		}

		return bs;
	}
}
