#include "TestSink.h"
#include "Logger.h"
#include <iostream>
#include <assert.h>

/*
	This class stores the last log message for unit test validation.
*/
using namespace std;

namespace LoggerLib
{
	CTestSink::CTestSink(Msg::Level level, std::string fmt) : CBaseSink(level, fmt)
	{
	}

	CTestSink::~CTestSink()
	{
	}

	bool CTestSink::deliver(Msg &msg)
	{
		message = getMessage(msg);
		return true;
	}
}

