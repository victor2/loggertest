#pragma once
#include "BaseSink.h"
#include <fstream>

namespace LoggerLib
{
	class CFileSink :
		public CBaseSink
	{
		std::ofstream m_os;
	public:
		CFileSink(std::string filename, Msg::Level level, std::string fmt);
		virtual ~CFileSink();
		
		bool deliver(Msg &msg);
	};
}
