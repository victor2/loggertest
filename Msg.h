#pragma once
#include <chrono>
#include <string>
#include <map>

namespace LoggerLib
{
	struct Msg
	{
		std::string msg;

		enum Level
		{
			NO_MSG = 0,
			DEBUG_MSG = 10,
			INFO_MSG = 20,
			WARNING_MSG = 30,
			ERROR_MSG = 40
		} level = NO_MSG;

		// Note that toString and fromString are not reversable.
		// The former adds whitespaces
		static std::string toString(Level lev)
		{
			switch (lev)
			{
			case NO_MSG:
				return " ";
			case DEBUG_MSG:
				return " DBG ";
			case INFO_MSG:
				return " INFO ";
			case WARNING_MSG:
				return " WARN ";
			case ERROR_MSG:
				return " ERR ";
			default:
				break;
			}
			return " XXX ";
		}

		static Level fromString(std::string s)
		{
			const std::map<std::string, Level> mapping = { 
				{ "debug", DEBUG_MSG },
				{ "info", INFO_MSG },
				{ "warn", WARNING_MSG },
				{ "error", ERROR_MSG }
			};

			return (*mapping.find(s)).second;
		}

		std::chrono::time_point<std::chrono::system_clock> clk;

		Msg(Level lev, std::string message)
		{
			clk = std::chrono::system_clock::now();
			msg = message;
			level = lev;
		}

		Msg()
		{
			level = NO_MSG;
		}

		bool isValid() { return level != NO_MSG; }
	};
}
