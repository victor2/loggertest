﻿#include "Logger.h"
#include <iomanip>

#define MAX_LOG_LINE 1024

namespace LoggerLib
{

	CLoggerCore CLogger::m_core;

	// Do we need to generate timestamp in constructor or destructor?
	// Hoping the difference is neglectable; it is done in desctructor where Msg is created.
	CLogger::CLogger() : std::ostream(this), MaxLogLine(MAX_LOG_LINE)
	{

	}

	CLogger::~CLogger()
	{
		(*this) << std::endl;
		std::string message(this->str(), this->pcount());
		this->freeze(false);

		Msg msg(m_level, m_context.empty() ? message : m_context + " " + message);
		m_core.push(msg);
	}
	
	CBaseSink *CLogger::createSink(std::map<std::string, std::string> &params)
	{
		return m_core.createSink(params);
	}

	CLogger &CLogger::printf(const char* format, ...)
	{
		char buffer[MAX_LOG_LINE];
		va_list argptr;
		va_start(argptr, format);
		vsnprintf(buffer, MAX_LOG_LINE, format, argptr);
		va_end(argptr);
		(*this) << buffer;
		return *this;
	}

	CLogger &CLogger::hex(void * data, int length)
	{
		(*this) << std::hex << std::setfill('0');
		for (int i = 0; i < length; i++)
		{
			(*this) << std::setw(2) << (int)((unsigned char*)data)[i] << " ";
		}
		return *this;
	}

}
