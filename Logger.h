#pragma once
#include <ostream>
#include <strstream>
#include <chrono>
#include "LoggerCore.h"

namespace LoggerLib
{
	// Each instance of this class produces exactly one log message
	class CLogger :
		public std::ostream, std::strstreambuf
	{
		static CLoggerCore m_core;

		// We might want to capture timestamp in constructor
		//		std::chrono::time_point<std::chrono::system_clock> m_clk; 
		Msg::Level m_level;
		std::string m_context;

		const int MaxLogLine;

	public:
		CLogger();
		virtual ~CLogger();

		// this reloads LoggerLib configuration 
		void restart() { m_core.init(); }

		CLogger &printf(const char* format, ...);
		CLogger &hex(void * data, int length);

		// these setters are used in macros defined below
		CLogger &setLevel(Msg::Level level) { m_level = level; return *this; }
		CLogger &setContext(std::string cnt) { m_context = cnt; return *this; }

		// this is exposed for unit testing - we need a way to add TestSink and
		// then adjust its log level, format, and check its last message stored.
		// TODO to make this private, create a 'friend' unit testing class
		static CBaseSink *createSink(std::map<std::string, std::string> &params);
	};
}

// This trick makes it possible to serialize user data to CLogger's ostream
template<class T>
auto operator<<(std::ostream& os, const T& t) -> decltype(t.print(os), os)
{
	t.print(os);
	return os;
}

// TODO add line number to debug message context
#define Debug LoggerLib::CLogger().setLevel(LoggerLib::Msg::DEBUG_MSG).setContext(__FUNCTION__)
#define Info LoggerLib::CLogger().setLevel(LoggerLib::Msg::INFO_MSG)
#define Warn LoggerLib::CLogger().setLevel(LoggerLib::Msg::WARN_MSG)
#define Error LoggerLib::CLogger().setLevel(LoggerLib::Msg::ERROR_MSG)
