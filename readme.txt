========================================================================
    MAKEFILE PROJECT : LoggerLib Project Overview
========================================================================

REQUIREMENTS

- Purpose: to distribute log lines produced by calling application to all configured output streams / sinks
- for each log function call, add line prefix and EOL suffix to the log line
- line prefix comprises:
-- time stamp with configurable format
-- log level (debug, info, warning, error)
-- [optional] context

- configuration specifies:
-- list of sinks
-- timestamp format, e.g. %H:%M:%S - specified per sink
-- log level - also per sink

- sink types: file, console, network, syslog

- log file rotation
- disc space check

- emergency actions / error handling
-- network sink timeout - 
-- internal queue overflow

- API style: some people prefer stream-oriented << operator, some prefer printf() style - the logger should have both.

- nice to have: it should be possible to change log level without restarting running application

CONSIDERATIONS

1. Each log line is produced by a single operator. Thus, an object with an operator scope can be used to insert
line prefix in the object constructor and suffix in desctructor:

CLogger() << "log message";

2. Log level can be set with a method that returns a reference to 'this':

CLogger().info() << "info message";

3. This can be simplified with preprocessor:

#define Info CLogger().info()

Info << "info message";

4. To change log level in runtime, we would use signals in Linux to reload configuration. 
In Windows we'd need sockets or polling configuration file timestamp in a dedicated thread,
or having a separate log server application or service running. Restarting the log server would reload configuration.

5. Log context can be set with static method CLogger::context(const char*) - but how to make it thread specific?

DESIGN

Multiple producers -> blocking queue -> single consumer -> multiple sinks.
A dedicated thread is needed for the consumer; its flow is controlled by blocking queue (it waits until the queue is not empty).

Producers are implemented by CLogger class. The blocking queue and consumer are in CLoggerCore class. 
The core copies log messages to all sinks via their std::ostream interface.

/////////////////////////////////////////////////////////////////////////////
