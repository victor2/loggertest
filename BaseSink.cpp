#include <chrono>
#include <ctime>
#include <iomanip>
#include "BaseSink.h"


using namespace std;
using namespace std::chrono;

namespace LoggerLib
{

	CBaseSink::CBaseSink(Msg::Level level, string fmt) : m_level(level), m_format(fmt)
	{
	}

	CBaseSink::~CBaseSink()
	{
	}

	string CBaseSink::getTimeStamp(time_point<system_clock> clk)
	{
		time_t time = system_clock::to_time_t(clk);
		tm timetm = *localtime(&time);
		char stamp[256];
		int n = strftime(stamp, sizeof(stamp), m_format.c_str(), &timetm);
		long long d = duration_cast<milliseconds>(clk.time_since_epoch()).count() % 1000;
		sprintf(stamp + n, ".%03lld", d);
		return stamp;
	}

	std::string	CBaseSink::getMessage(Msg &msg)
	{
		return getTimeStamp(msg.clk) + Msg::toString(msg.level) + msg.msg;
	}

}