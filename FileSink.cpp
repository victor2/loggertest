#include "FileSink.h"

using namespace std;

namespace LoggerLib
{
	CFileSink::CFileSink(string filename, Msg::Level level, string fmt) : CBaseSink(level, fmt)
	{
		m_os.open(filename.c_str(), ofstream::out | ofstream::app);
	}

	CFileSink::~CFileSink()
	{

	}

	bool CFileSink::deliver(Msg &msg)
	{
		if (m_os.is_open())
		{
			m_os << getMessage(msg);
			m_os.flush();
			return true;
		}
		return false;
	}
}

