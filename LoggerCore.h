#pragma once
#include <iostream>
#include <queue>
#include <list>
#include <mutex>
#include <thread>
#include <Windows.h>
#include "BaseSink.h"
#include "Msg.h"

namespace LoggerLib
{
	class CLoggerCore
	{
		// private constructor
		CLoggerCore();
		CBaseSink *createSink(std::map<std::string, std::string> &params);

	protected:
		// The message queue and synchronization means 
		std::queue<Msg> m_queue;
		std::mutex m_mutex;
		std::condition_variable m_not_empty;

		// Consumer thread and a flag to stop it
		std::thread m_thread;
		bool m_stopped;

		// The sinks to deliver log messages to
		std::list<CBaseSink*> m_sinks;

		// To get configuration from ini file
		static LPCSTR m_iniFile;
		static std::vector<std::string> getSinks();
		static std::string getGlobal(LPCSTR key, LPCSTR deflt = "");
		static std::string getSinkParam(LPCSTR sink, LPCSTR key, LPCSTR deflt = "");

	public:
		~CLoggerCore();

		void init();
		void start();
		void stop();
		void work();

		// this copies a log message to a queue and notifies the consumer
		void push(Msg &msg);

		// the only class that can create an instance of this Core
		friend class CLogger;
	};
}
