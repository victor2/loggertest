#pragma once
#include "BaseSink.h"

namespace LoggerLib
{
	class CTestSink :
		public CBaseSink
	{
	public:
		CTestSink(Msg::Level level, std::string fmt);
		virtual ~CTestSink();

		bool deliver(Msg &msg);

		// the last log message is stored here. Easy to access in unit tests.
		std::string message;
	};

}

