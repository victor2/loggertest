#pragma once
#include <ostream>
#include "Msg.h"

namespace LoggerLib
{
	class CBaseSink
	{
	protected:
		std::string		m_format;
		Msg::Level		m_level;

		std::string	getTimeStamp(std::chrono::time_point<std::chrono::system_clock> clk);
		std::string	getMessage(Msg &msg);

	public:
		CBaseSink(Msg::Level level, std::string fmt);
		virtual ~CBaseSink();

		Msg::Level getLevel() { return m_level; }
		void setLevel(Msg::Level lev) { m_level = lev; }
		virtual bool deliver(Msg &msg) = 0;
	};
}